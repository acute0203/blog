---
title: MLP/DNN 推導及流程
date: 2018-02-09 20:45:28
categories: Deep Learning
tags: [PDF,Deep Learning,A.I.,Data Science]
---
全連結層的深度類神經網路介紹和BP推導。
<!--more-->
{% pdf ./MLP-DNN-推導及流程.pdf %}
